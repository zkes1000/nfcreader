﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fc读卡器程序
{
   public class model
    {
       private string _id = string.Empty;

       /// <summary>
       /// ID
       /// </summary>
       public string ID 
       {
           get {
               return _id;
           }
           set
           {
               _id = value;
           }
       }

       private string _numhouseholds = string.Empty;
       /// <summary>
       /// Numhouseholds
       /// </summary>
       public string Numhouseholds
       {
           get
           {
               return _numhouseholds;
           }
           set
           {
               _numhouseholds = value;
           }
       }
       private string _nfcid = string.Empty;
       /// <summary>
       /// ID
       /// </summary>
       public string NFCID
       {
           get
           {
               return _nfcid;
           }
           set
           {
               _nfcid = value;
           }
       }
    }
}
