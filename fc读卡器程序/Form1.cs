﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPOI.HSSF;

using System.Runtime.InteropServices;
using System.IO;
using System.Data.OleDb;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace fc读卡器程序
{
    public partial class NFC_PCWriter : Form
    {
        [DllImport("kernel32.dll")]
        static extern void Sleep(int dwMilliseconds);


        //=========================== System Function =============================
        /// <summary>
        /// 获取连接到pc机的设备数
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="pNum"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_GetDeviceNum(UInt16 vid, UInt16 pid, ref UInt32 pNum);

        /// <summary>
        /// 通过设备索引号、VID、PID获取设备序列号字符串描述符
        /// </summary>
        /// <param name="deviceIndex"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="deviceString"></param>
        /// <param name="deviceStringLength"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_GetHidSerialNumberStr(UInt32 deviceIndex,
                                                    UInt16 vid,
                                                    UInt16 pid,
                                                    ref Char deviceString,
                                                    UInt32 deviceStringLength);
        /// <summary>
        /// 打开HID设备
        /// </summary>
        /// <param name="device"></param>
        /// <param name="index"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_Open(ref IntPtr device,
                                   UInt32 index,
                                   UInt16 vid,
                                   UInt16 pid);

        /// <summary>
        /// 查询设备是否已经打开
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern bool Sys_IsOpen(IntPtr device);

        /// <summary>
        /// 关闭设备
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_Close(ref IntPtr device);

        /// <summary>
        /// 指示灯颜色 0 熄灭 1 红色 2 绿色 3 黄色
        /// </summary>
        /// <param name="device"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_SetLight(IntPtr device, byte color);

        /// <summary>
        /// 设置蜂鸣
        /// </summary>
        /// <param name="device"></param>
        /// <param name="msec"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_SetBuzzer(IntPtr device, byte msec);

        [DllImport("hfrdapi.dll")]
        static extern int Sys_SetAntenna(IntPtr device, byte mode);

        /// <summary>
        /// 设置读写器非接触工作方式 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int Sys_InitType(IntPtr device, byte type);


        //=========================== M1 Card Function =============================
        /// <summary>
        /// 寻T读卡器上面的卡片 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="mode"></param>
        /// <param name="pTagType"></param>
        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int TyA_Request(IntPtr device, byte mode, ref UInt16 pTagType);


        /// <returns></returns>
        [DllImport("hfrdapi.dll")]
        static extern int TyA_NTAG_AnticollSelect(IntPtr device, byte[] pSnr, ref byte pLen);


        [DllImport("hfrdapi.dll")]
        static extern int TyA_NTAG_Read(IntPtr device,
                                      byte addr,
                                      byte[] pData,
                                      ref byte pLen);

        [DllImport("hfrdapi.dll")]
        static extern int TyA_NTAG_Write(IntPtr device, byte addr, byte[] pData);


        [DllImport("hfrdapi.dll")]
        static extern int TyA_NTAG_PwdAuth(IntPtr device,
                             byte[] pPwd,
                             byte[] pData,
                             ref byte pLen);

        //==========================================================================
        IntPtr g_hDevice = (IntPtr)(-1); //g_hDevice must init as -1

        static char[] hexDigits = { 
            '0','1','2','3','4','5','6','7',
            '8','9','A','B','C','D','E','F'};

        //*------------------ PWD setting -----------------------------------------*//
        byte[] NFCPWD = { 0x1A, 0x1B, 0x1C, 0x1D };
        byte[] NFCPACK = { 0xA1, 0xB1 };

        //*------------------ Writting Para Setting -----------------------------------------*//
        byte AddrData = 0x04;
        int PageNum4Wrt = 4; //4 pages need for writting, also 16 bytes, also 128 bits

        public static byte GetHexBitsValue(byte ch)
        {
            byte sz = 0;
            if (ch <= '9' && ch >= '0')
                sz = (byte)(ch - 0x30);
            if (ch <= 'F' && ch >= 'A')
                sz = (byte)(ch - 0x37);
            if (ch <= 'f' && ch >= 'a')
                sz = (byte)(ch - 0x57);

            return sz;
        }
        //
        //数据源转化成list
        private static DataTable csdt = new DataTable();

        //已经插入成功的数据
        private static List<Successmodel> SuccessList = new List<Successmodel>();

        private static string excelpath = "";
        #region byteHEX
        /// <summary>
        /// 单个字节转字字符.
        /// </summary>
        /// <param name="ib">字节.</param>
        /// <returns>转换好的字符.</returns>
        public static String byteHEX(Byte ib)
        {
            String _str = String.Empty;
            try
            {
                char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
			    'B', 'C', 'D', 'E', 'F' };
                char[] ob = new char[2];
                ob[0] = Digit[(ib >> 4) & 0X0F];
                ob[1] = Digit[ib & 0X0F];
                _str = new String(ob);
            }
            catch (Exception)
            {
                new Exception("对不起有错。");
            }
            return _str;

        }
        #endregion

        public static string ToHexString(byte[] bytes)
        {
            String hexString = String.Empty;
            for (int i = 0; i < bytes.Length; i++)
                hexString += byteHEX(bytes[i]);

            return hexString;
        }



        public static byte[] ToDigitsBytes(string theHex)
        {
            byte[] bytes = new byte[theHex.Length / 2 + (((theHex.Length % 2) > 0) ? 1 : 0)];
            for (int i = 0; i < bytes.Length; i++)
            {
                char lowbits = theHex[i * 2];
                char highbits;

                if ((i * 2 + 1) < theHex.Length)
                    highbits = theHex[i * 2 + 1];
                else
                    highbits = '0';

                int a = (int)GetHexBitsValue((byte)lowbits);
                int b = (int)GetHexBitsValue((byte)highbits);
                bytes[i] = (byte)((a << 4) + b);
            }

            return bytes;
        }

        public NFC_PCWriter()
        {
            InitializeComponent();
            dataGridView1.DataSource = createSouce();
            this.dataGridView1.Columns[0].FillWeight = 32;      //第一列的相对宽度为10%
            this.dataGridView1.Columns[1].FillWeight = 32;      //第二列的相对宽度为20%
            this.dataGridView1.Columns[2].FillWeight = 32;      //第三列的相对宽度为30%
            this.ConnectBtn.Enabled = false;
            this.WrtOneDataBtn.Enabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectBtn_click(object sender, EventArgs e)
        {
            int status;
            string strError;

            //=========================== Connect reader =========================
            //Check whether the reader is connected or not
            if (true == Sys_IsOpen(g_hDevice))
            {
                //If the reader is already open , close it firstly
                status = Sys_Close(ref g_hDevice);
                if (0 != status)
                {
                    strError = "Sys_Close failed !";
                    writein_messagebox(strError);
                    return;
                }
            }

            //Connect
            status = Sys_Open(ref g_hDevice, 0, 0x0416, 0x8020);
            if (0 != status)
            {
                strError = "Sys_Open failed !";
                writein_messagebox(strError);
                return;
            }


            //============= Init the reader before operating the card ============
            //Close antenna of the reader
            status = Sys_SetAntenna(g_hDevice, 0);
            if (0 != status)
            {
                strError = "Sys_SetAntenna failed !";
                writein_messagebox(strError);
                return;
            }
            Sleep(5); //Appropriate delay after Sys_SetAntenna operating 

            //Set the reader's working mode
            status = Sys_InitType(g_hDevice, (byte)'A');
            if (0 != status)
            {
                strError = "Sys_InitType failed !";
                writein_messagebox(strError);
                return;
            }
            Sleep(5); //Appropriate delay after Sys_InitType operating

            //Open antenna of the reader
            status = Sys_SetAntenna(g_hDevice, 1);
            if (0 != status)
            {
                strError = "Sys_SetAntenna failed !";
                writein_messagebox(strError);
                return;
            }
            Sleep(5); //Appropriate delay after Sys_SetAntenna operating


            //============================ Success Tips ==========================
            //Beep 200 ms
            status = Sys_SetBuzzer(g_hDevice, 20);
            if (0 != status)
            {
                strError = "Sys_SetBuzzer failed !";
                writein_messagebox(strError);
                return;
            }
            else
            {
                writein_messagebox("连接读卡器成功！");
                this.WrtOneDataBtn.Enabled = true;
            }
        }


        /// <summary>
        /// 读取当前卡的卡号，并写入卡片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WrtOneDataBtn_click(object sender, EventArgs e)
        {
            if (SuccessList == null || SuccessList.Count() <= 0)
            {
                writein_messagebox("请载入初始数据！");
            }
            else
            {
                int a = dataGridView1.CurrentRow.Index;
                string curdata = dataGridView1.Rows[a].Cells[2].Value.ToString();
                string addres = dataGridView1.Rows[a].Cells[1].Value.ToString();
                if (OneWriteBtn_Click(curdata))
                {
                    // buzzer 300ms
                    Sys_SetBuzzer(g_hDevice, 30);
                    
                    writein_messagebox("写入" + addres + " 成功！！！！");
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Gray;
                    foreach (var item in SuccessList)
                    {
                        if (item.ID == dataGridView1.Rows[a].Cells[0].Value.ToString())
                        {
                            item.State = 1;
                            item.Datetime = DateTime.Now.ToString();
                        }
                    }
                    if (a < dataGridView1.RowCount-1)
                    {
                        dataGridView1.CurrentCell = dataGridView1.Rows[a + 1].Cells[0];
                        this.dataGridView1.Rows[a].Selected = false;
                        this.dataGridView1.Rows[a + 1].Selected = true;
                        string address = dataGridView1.Rows[a+1].Cells[1].Value.ToString();
                        this.Select_Title.Text = "当前操作: " + address;
                    }
                    //SuccessList.Add(addres);
                    LogHelper.Writelog("写入" + addres + " 成功！！！！");
                }
                else
                {
                    // buzzer twice
                    Sys_SetBuzzer(g_hDevice, 15);
                    Sleep(300);
                    Sys_SetBuzzer(g_hDevice, 15);
             
                    writein_messagebox("写入" + addres + " 失败！！！！");
                    dataGridView1.Rows[a].DefaultCellStyle.BackColor = Color.Red;
                    foreach (var item in SuccessList)
                    {
                        if (item.ID == dataGridView1.Rows[a].Cells[0].Value.ToString())
                        {
                            item.State = 2;
                            item.Datetime = DateTime.Now.ToString();
                        }
                    }
                    LogHelper.Writelog("写入" + addres + " 失败！！！！");
                }
            }
        }

        /// <summary>
        /// 写入信息板
        /// </summary>
        /// <param name="str"></param>
        private void writein_messagebox(string str)
        {
            this.showmessagebox.Text = str + "-------------------" + DateTime.Now.ToString() + "\r\n" + this.showmessagebox.Text;
        }

        /// <summary>
        /// 载入excel数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadDataBtn_click(object sender, EventArgs e)
        {
            string resultFile;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "Excel文件|*.xls;*.xlsx;*.csv;"; 
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFile = openFileDialog1.FileName;
                if (IsFileInUse(resultFile))
                {
                    writein_messagebox("该文件正在被使用，请确保文件未被打开！！！");
                    MessageBox.Show("该文件正在被使用，请确保文件未被打开！！！");
                    return;
                }
                //保存excel路径
                excelpath = resultFile;
                ExcelToDS(resultFile);
                //载入数据
                listtodt(SuccessList);
                //初始化表格状态
                ResetTableColor(SuccessList);
                for (int i = 0; i < this.dataGridView1.Columns.Count; i++)
                {
                    this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    //this.dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                }
                this.dataGridView1.Columns[0].FillWeight = 32;      //第一列的相对宽度为10%
                this.dataGridView1.Columns[1].FillWeight = 32;      //第二列的相对宽度为20%
                this.dataGridView1.Columns[2].FillWeight = 32;      //第三列的相对宽度为30%               
                if (dataGridView1.DataSource != null && dataGridView1.Rows.Count > 1)
                {
                    writein_messagebox("初始数据载入成功。");
                    string addres = dataGridView1.Rows[0].Cells[1].Value.ToString();
                    this.Select_Title.Text = "当前操作: " + addres;
                    //csdt = ds.Tables[0];
                    LogHelper.Writelog("初始数据载入成功!");
                    this.ConnectBtn.Enabled = true;
                }
                else
                {

                }
            }
        }

        /// <summary>
        /// 将初始list转话成dt放入远数据
        /// </summary>
        private void listtodt(List<Successmodel> model)
        {
            DataTable dt = new DataTable();//创建DataTable对象  
            dt.Columns.Add("索引", System.Type.GetType("System.Int32"));
            DataColumn dc = new DataColumn("住户编号", System.Type.GetType("System.String"));
            dt.Columns.Add(dc);//添加1字符串列  
            DataColumn dc2 = new DataColumn("NFC ID", System.Type.GetType("System.String"));
            dt.Columns.Add(dc2);//添加1字符串列
            if (model != null && model.Count() > 0)
            {
                foreach (var item in model)
                {
                    if (!string.IsNullOrWhiteSpace(item.ID))
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = item.ID.ToString();
                        dr[1] = item.Numhouseholds.ToString();
                        dr[2] = item.NFCID.ToString();
                        dt.Rows.Add(dr);
                    }
                }
            }
            dataGridView1.DataSource = dt;
        }
        /// <summary>
        /// 发现FNC卡片
        /// </summary>
        /// <returns></returns>
        public bool FindActNFC()
        {
            int status = 0;
            byte mode = 0x26; //0x52
            ushort TagType = 0;
            byte[] dataBuffer = new byte[256];
            byte len = 255;

            for (int i = 0; i < 2; i++)
            {
                status = TyA_Request(g_hDevice, mode, ref TagType);
                if (status != 0)
                    continue;

                status = TyA_NTAG_AnticollSelect(g_hDevice, dataBuffer, ref len); //Return the card serial number
                if (status != 0)
                    continue;

                /*String m_cardNo = String.Empty;

                for (int q = 0; q < len; q++)
                {
                    m_cardNo += byteHEX(dataBuffer[q]);
                }
                txtSearchPurse.Text = m_cardNo;
                */
                break;
            }
            if (status != 0)
            {
                writein_messagebox("NFC card can not be found or failed to be activiated !");
                return false;
            }
            return true;
        }

        private bool OneWriteBtn_Click(string Cur_IDString)
        {

            if (true != Sys_IsOpen(g_hDevice))
            {
                writein_messagebox("Not connect to device !");
                return false;
            }

            int status = 0;

            // search nfc and activate it
            if (!FindActNFC())
            {
                return false;
            }

            // pwd authentication
            byte[] Pack = new byte[2];
            byte pack_len = 0;
            byte[] WhitePWD = { 0xFF, 0xFF, 0xFF, 0xFF };

            status = TyA_NTAG_PwdAuth(g_hDevice, WhitePWD, Pack, ref pack_len);
            if (status == 0) // is "white card"
            {
                if (!InitWhiteCard())
                {
                    return false;
                }

            }
            else // already pwd-written in past time
            {

                if (!FindActNFC())
                {
                    return false;
                }
                // set 
                status = TyA_NTAG_PwdAuth(g_hDevice, NFCPWD, Pack, ref pack_len);

                if (0 != status)
                {
                    writein_messagebox("NFC Pwd Auth Failed.");
                    return false;
                }
            }

            // write the data 
            byte[] data2wrt = ToDigitsBytes(Cur_IDString);
            if (data2wrt.Length != PageNum4Wrt * 4)  // count checking
            {
                writein_messagebox("Illegal data length to be written.");
                return false;
            }
            byte AddrData2Wrt = AddrData;
            byte[] Data_t = new byte[4];
            for (int i = 0; i < PageNum4Wrt; i++)
            {
                Data_t[0] = data2wrt[4 * i];
                Data_t[1] = data2wrt[4 * i + 1];
                Data_t[2] = data2wrt[4 * i + 2];
                Data_t[3] = data2wrt[4 * i + 3];

                status = TyA_NTAG_Write(g_hDevice, AddrData2Wrt, Data_t);
                if (status != 0)
                {
                    MessageBox.Show("Page @" + AddrData.ToString() + " Written Failed!", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                AddrData2Wrt++; // next page
            }

            // read and compare
            byte[] dataBuffer = new byte[256];
            byte cLen = 0;
            status = TyA_NTAG_Read(g_hDevice, AddrData, dataBuffer, ref cLen);
            if (status != 0 || cLen != 16)
            {
                writein_messagebox("Data read failed !");
                return false;
            }
            String ReadStr;
            byte[] bytesData = new byte[cLen];
            for (int i = 0; i < cLen; i++)
            {
                bytesData[i] = Marshal.ReadByte(dataBuffer, i);
            }
            ReadStr = ToHexString(bytesData);
            if (0 != String.Compare(ReadStr, Cur_IDString, true))
            {
                writein_messagebox("Data Read and Comapre Failed !");
                return false;
            }


            return true;
        }

        /// <summary>
        /// 将数据存入初始的list中
        /// </summary>
        /// <param name="Path"></param>
        public void ExcelToDS(string Path)
        {          
            try
            {
                StringBuilder sbr = new StringBuilder();
                using (FileStream fs = File.OpenRead(Path))   //打开myxls.xls文件
                {
                    SuccessList = new List<Successmodel>();
                    if (Path.IndexOf(".xlsx") > 0) // 2007版本
                    {
                        XSSFWorkbook wk = new XSSFWorkbook(fs);   //把xls文件中的数据写入wk中
                        for (int i = 0; i < wk.NumberOfSheets; i++)  //NumberOfSheets是myxls.xls中总共的表数
                        {
                            ISheet sheet = wk.GetSheetAt(i);   //读取当前表数据
                            for (int j = 0; j <= sheet.LastRowNum; j++)  //LastRowNum 是当前表的总行数
                            {
                                IRow row = sheet.GetRow(j);  //读取当前行数据
                                if (j == 0)
                                {
                                    continue;
                                }
                                else if (row != null)
                                {
                                    Successmodel model = new Successmodel();
                                    model.ID = row.GetCell(0).ToString();
                                    model.Numhouseholds = row.GetCell(1).ToString();
                                    model.NFCID = row.GetCell(2).ToString();
                                    if (row.LastCellNum >= 4)
                                    {
                                        model.State = Convert.ToInt32(row.GetCell(3).ToString());
                                    }
                                    else
                                    {
                                        model.State = 0;
                                    }
                                    if (row.LastCellNum >= 5)
                                    {
                                        model.Datetime = row.GetCell(4).ToString();
                                    }
                                    else
                                    {
                                        model.Datetime = "";
                                    }
                                    SuccessList.Add(model);
                                }
                            }
                        }
                    }
                    else if (Path.IndexOf(".xls") > 0) // 2003版本
                    {
                        HSSFWorkbook wk = new HSSFWorkbook(fs);   //把xls文件中的数据写入wk中
                        for (int i = 0; i < wk.NumberOfSheets; i++)  //NumberOfSheets是myxls.xls中总共的表数
                        {
                            ISheet sheet = wk.GetSheetAt(i);   //读取当前表数据
                            for (int j = 0; j <= sheet.LastRowNum; j++)  //LastRowNum 是当前表的总行数
                            {
                                IRow row = sheet.GetRow(j);  //读取当前行数据
                                if (j == 0)
                                {
                                    continue;
                                }
                                else if (row != null)
                                {
                                    Successmodel model = new Successmodel();
                                    model.ID = row.GetCell(0).ToString();
                                    model.Numhouseholds = row.GetCell(1).ToString();
                                    model.NFCID = row.GetCell(2).ToString();
                                    if (row.LastCellNum >= 4)
                                    {
                                        model.State = Convert.ToInt32(row.GetCell(3).ToString());
                                    }
                                    else
                                    {
                                        model.State = 0;
                                    }
                                    if (row.LastCellNum >= 5)
                                    {
                                        model.Datetime = row.GetCell(4).ToString();
                                    }
                                    else
                                    {
                                        model.Datetime = "";
                                    }
                                    SuccessList.Add(model);
                                }
                            }
                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        FileStream fscsv = new FileStream(Path, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                        //StreamReader sr = new StreamReader(fs, Encoding.UTF8);
                        StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
                        //string fileContent = sr.ReadToEnd();
                        //encoding = sr.CurrentEncoding;
                        //记录每次读取的一行记录
                        string strLine = "";
                        //记录每行记录中的各字段内容
                        string[] aryLine = null;
                        string[] tableHead = null;
                        //标示列数
                        int columnCount = 0;
                        //标示是否是读取的第一行
                        bool IsFirst = true;
                        //逐行读取CSV中的数据
                        while ((strLine = sr.ReadLine()) != null)
                        {
                            //strLine = Common.ConvertStringUTF8(strLine, encoding);
                            //strLine = Common.ConvertStringUTF8(strLine);

                            if (IsFirst == true)
                            {
                                tableHead = strLine.Split(',');
                                IsFirst = false;
                                columnCount = tableHead.Length;
                                //创建列
                                for (int i = 0; i < columnCount; i++)
                                {
                                    DataColumn dc = new DataColumn(tableHead[i]);
                                    dt.Columns.Add(dc);
                                }
                            }
                            else
                            {
                                aryLine = strLine.Split(',');
                                DataRow dr = dt.NewRow();
                                for (int j = 0; j < columnCount; j++)
                                {
                                    dr[j] = aryLine[j];
                                }
                                dt.Rows.Add(dr);
                            }
                        }
                        if (aryLine != null && aryLine.Length > 0)
                        {
                            dt.DefaultView.Sort = tableHead[0] + " " + "asc";
                        }
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                Successmodel model = new Successmodel();
                                model.ID = dt.Rows[i][0].ToString();
                                model.Numhouseholds = dt.Rows[i][1].ToString();
                                model.NFCID = dt.Rows[i][2].ToString();
                                model.State = Convert.ToInt32(dt.Rows[i][3].ToString() == "" ? "0" : dt.Rows[i][3].ToString());
                                model.Datetime = dt.Rows[i][4].ToString();
                                SuccessList.Add(model);
                            }
                        }
                        sr.Close();
                        fscsv.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                writein_messagebox(ex.ToString());
            }                      
        }

        /// <summary>
        /// 初始化数据表
        /// </summary>
        /// <returns></returns>
        private DataTable createSouce()//创建数据源  
        {
            DataTable dt = new DataTable();//创建DataTable对象  
            dt.Columns.Add("索引", System.Type.GetType("System.Int32"));
            DataColumn dc = new DataColumn("住户编号", System.Type.GetType("System.String"));
            dt.Columns.Add(dc);//添加1字符串列  
            DataColumn dc2 = new DataColumn("NFC ID", System.Type.GetType("System.String"));
            dt.Columns.Add(dc2);//添加1字符串列  
            return dt;
        }

        /// <summary>
        /// 验证是否是白卡
        /// </summary>
        /// <returns></returns>
        public bool InitWhiteCard()
        {
            int status;

            if (true != Sys_IsOpen(g_hDevice))
            {
                writein_messagebox("Not connect to device !");
                return false;
            }

            byte AddrCFG0 = 0x29;
            byte AddrCFG1 = 0x2A;

            // write CFG0
            byte[] CFG0 = new byte[4];
            CFG0[0] = 0x04; // STRG_MOD_EN 1 1b STRG MOD_EN defines the modulation mode
            CFG0[1] = 0x00; // RFUI
            CFG0[2] = 0x00; // MIRROR_PAGE
            CFG0[3] = 0x04; // AUTH0 defines the page address from which the password verification is required. 
            // PWD verification is necessary when the page ranging from 04x to end is to be accessed.
            status = TyA_NTAG_Write(g_hDevice, AddrCFG0, CFG0);
            if (status != 0)
            {
                writein_messagebox("CFG0 Write Failed !");
                return false;
            }

            // wirte CFG1
            byte[] CFG1 = new byte[4];
            CFG1[0] = 0x40; // PROT=0 (write access is protected by the password verification);
            // CFGLCK=1 (user configuration permanently locked against write access, except PWD and PACK)
            CFG1[1] = 0x00; // RFUI
            CFG1[2] = 0x00; // RFUI
            CFG1[3] = 0x00; // RFUI 
            status = TyA_NTAG_Write(g_hDevice, AddrCFG1, CFG1);
            if (status != 0)
            {
                writein_messagebox("CFG1 Write Failed !");
                return false;
            }

            // write PWD
            byte AddrPWD = 0x2B;
            status = TyA_NTAG_Write(g_hDevice, AddrPWD, NFCPWD);
            if (status != 0)
            {
                writein_messagebox("NFC Password Write Failed !");
                return false;
            }

            // write PACK
            byte AddrPACK = 0x2C;
            byte[] bytesto2C = new byte[4];
            bytesto2C[0] = NFCPACK[0];
            bytesto2C[1] = NFCPACK[1];
            bytesto2C[2] = 0x00;
            bytesto2C[3] = 0x00;
            status = TyA_NTAG_Write(g_hDevice, AddrPACK, bytesto2C);
            if (status != 0)
            {
                writein_messagebox("Acknowledge data Write Failed !");
                return false;
            }

            return true;

        }

        private void Cell_onclick(object sender, DataGridViewCellEventArgs e)
        {
            int a = dataGridView1.CurrentRow.Index;
            //string str = dataGridView1.Rows[a].Cells[2].Value.ToString();
            string addres = dataGridView1.Rows[a].Cells[1].Value.ToString();
            //this.Select_Title.Text = "您选择了住户编号为-" + addres + "-这条数据";
            this.Select_Title.Text = "当前操作: " + addres;
        }

        private void Search_Click(object sender, EventArgs e)
        {
            string searchkey = this.searchbox.Text;
            if (!string.IsNullOrWhiteSpace(searchkey))
            {
                //linq查询
                List<Successmodel> successListsearch = (from successmodel in SuccessList
                                                        where successmodel.Numhouseholds.Contains(searchkey)
                                                        select successmodel).ToList<Successmodel>();
                if (successListsearch != null && successListsearch.Count() > 0)
                {
                    listtodt(successListsearch);
                    ResetTableColor(successListsearch);
                    string addres = dataGridView1.Rows[0].Cells[1].Value.ToString();
                    this.Select_Title.Text = "当前操作： " + addres;
                    writein_messagebox("检索成功！");
                }
                else
                {
                    writein_messagebox("很抱歉未查询到相关数据！");
                }
            }
            else
            {
                listtodt(SuccessList);
                ResetTableColor(SuccessList);
                string addres = dataGridView1.Rows[0].Cells[1].Value.ToString();
                this.Select_Title.Text = "当前操作： " + addres;
                writein_messagebox("检索成功！");
            }
        }

        /// <summary>
        /// 初始化表格每行的颜色
        /// </summary>
        private void ResetTableColor(List<Successmodel> model)
        {
            if (model != null && model.Count() > 0)
            {
                int i = 0;
                foreach (var item in model)
                {
                    if (item.State == 0)
                    {

                    }
                    else if (item.State == 1)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                    }
                    else if (item.State == 2)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                    i++;
                }
            }
        }

        // <summary>
        /// 关闭窗口时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NFC_PCWriter_FormClosed(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("你确定要关闭吗！", "提示信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (IsFileInUse(excelpath))
            {
                if (string.IsNullOrWhiteSpace(excelpath))
                {
                    e.Cancel = false;
                    return;
                }
                writein_messagebox("该文件正在被使用，请确保文件未被打开！！！");
                MessageBox.Show("该文件正在被使用，请确保文件未被打开！！！");
                e.Cancel = true;
                return;
            }
            if (result == DialogResult.OK)
            {
                DataTable dt = new DataTable();//创建DataTable对象  
                dt.Columns.Add("索引", System.Type.GetType("System.Int32"));
                DataColumn dc = new DataColumn("住户编号", System.Type.GetType("System.String"));
                dt.Columns.Add(dc);//添加1字符串列  
                DataColumn dc2 = new DataColumn("NFC ID", System.Type.GetType("System.String"));
                dt.Columns.Add(dc2);//添加1字符串列
                DataColumn dc3 = new DataColumn("State", System.Type.GetType("System.String"));
                dt.Columns.Add(dc3);//添加1字符串列
                DataColumn dc4 = new DataColumn("Time", System.Type.GetType("System.String"));
                dt.Columns.Add(dc4);//添加1字符串列
                if (SuccessList != null && SuccessList.Count() > 0)
                {
                    foreach (var item in SuccessList)
                    {
                        if (!string.IsNullOrWhiteSpace(item.ID))
                        {
                            DataRow dr = dt.NewRow();
                            dr[0] = item.ID.ToString();
                            dr[1] = item.Numhouseholds.ToString();
                            dr[2] = item.NFCID.ToString();
                            dr[3] = item.State.ToString();
                            dr[4] = item.Datetime.ToString();
                            dt.Rows.Add(dr);
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(excelpath))
                {
                    ExportFile(dt, excelpath);
                }
                e.Cancel = false;  //点击OK   
            }
            else
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// 写入原导入的excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportFile(System.Data.DataTable dt, string fileName)
        {
            if (dt == null || dt.Rows.Count <= 0)
            {
                return false;
            }                      
            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    if (fileName.IndexOf(".xlsx") > 0 || fileName.IndexOf(".xls") > 0)
                    {
                        File.Delete(fileName);
                    }
                    else
                    {
                        File.Delete(fileName);
                    }
                }  
                if (DataTableToExcel(fileName, dt, true) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw (ex);              
            }            
        }
        private IWorkbook workbook = null;
        private FileStream fs = null;
        /// <summary>
        /// 将DataTable数据导入到excel中
        /// </summary>
        /// <param name="data">要导入的数据</param>
        /// <param name="isColumnWritten">DataTable的列名是否要导入</param>
        /// <param name="sheetName">要导入的excel的sheet的名称</param>
        /// <returns>导入数据行数(包含列名那一行)</returns>
        public int DataTableToExcel(string fileName, DataTable data,bool isColumnWritten)
        {
            int i = 0;
            int j = 0;
            int count = 0;
            ISheet sheet = null;           
            if (fileName.IndexOf(".xlsx") > 0 || fileName.IndexOf(".xls") > 0)
            {
                fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (fileName.IndexOf(".xlsx") > 0) // 2007版本
                    workbook = new XSSFWorkbook();
                else if (fileName.IndexOf(".xls") > 0) // 2003版本
                    workbook = new HSSFWorkbook();
                try
                {
                    if (workbook != null)
                    {
                        sheet = workbook.CreateSheet("Sheet1");
                    }
                    else
                    {
                        return -1;
                    }

                    if (isColumnWritten == true) //写入DataTable的列名
                    {
                        IRow row = sheet.CreateRow(0);
                        for (j = 0; j < data.Columns.Count; ++j)
                        {
                            row.CreateCell(j).SetCellValue(data.Columns[j].ColumnName);
                        }
                        count = 1;
                    }
                    else
                    {
                        count = 0;
                    }

                    for (i = 0; i < data.Rows.Count; ++i)
                    {
                        IRow row = sheet.CreateRow(count);
                        for (j = 0; j < data.Columns.Count; ++j)
                        {
                            row.CreateCell(j).SetCellValue(data.Rows[i][j].ToString());
                        }
                        ++count;
                    }
                    workbook.Write(fs); //写入到excel
                    return count;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);                   
                    return -1;
                    throw (ex);
                }
            }
            else
            {
                if (SaveCSV(data, fileName))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }
        /// <summary>
        /// 写入CSV文件
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="fileName">文件全名</param>
        /// <returns>是否写入成功</returns>
        public bool SaveCSV(DataTable dt, string fullFileName)
        {           
            Boolean r = false;
            FileStream fs = new FileStream(fullFileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
            string data = "";

            //写出列名称
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                data += dt.Columns[i].ColumnName.ToString();
                if (i < dt.Columns.Count - 1)
                {
                    data += ",";
                }
            }
            sw.WriteLine(data);

            //写出各行数据
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                data = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    data += dt.Rows[i][j].ToString();
                    if (j < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
            }

            sw.Close();
            fs.Close();

            r = true;
            return r;


        }

        /// <summary>
        /// 判断文件是否被占用
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool IsFileInUse(string fileName)
        {
            bool inUse = true;
            FileStream fs = null;
            try
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read,
                FileShare.None);
                inUse = false;
            }
            catch
            {
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            return inUse;//true表示正在使用,false没有使用    
        }    
    }
}
