﻿namespace fc读卡器程序
{
    partial class NFC_PCWriter
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NFC_PCWriter));
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.WrtOneDataBtn = new System.Windows.Forms.Button();
            this.LoadDataBtn = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.信息 = new System.Windows.Forms.GroupBox();
            this.showmessagebox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Select_Title = new System.Windows.Forms.Label();
            this.searchbox = new System.Windows.Forms.TextBox();
            this.Search = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ConnectBtn.Location = new System.Drawing.Point(12, 291);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(116, 45);
            this.ConnectBtn.TabIndex = 0;
            this.ConnectBtn.Text = "连接读卡器";
            this.ConnectBtn.UseVisualStyleBackColor = true;
            this.ConnectBtn.Click += new System.EventHandler(this.ConnectBtn_click);
            // 
            // WrtOneDataBtn
            // 
            this.WrtOneDataBtn.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.WrtOneDataBtn.Location = new System.Drawing.Point(12, 439);
            this.WrtOneDataBtn.Name = "WrtOneDataBtn";
            this.WrtOneDataBtn.Size = new System.Drawing.Size(116, 45);
            this.WrtOneDataBtn.TabIndex = 1;
            this.WrtOneDataBtn.Text = "写入数据";
            this.WrtOneDataBtn.UseVisualStyleBackColor = true;
            this.WrtOneDataBtn.Click += new System.EventHandler(this.WrtOneDataBtn_click);
            // 
            // LoadDataBtn
            // 
            this.LoadDataBtn.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LoadDataBtn.Location = new System.Drawing.Point(12, 159);
            this.LoadDataBtn.Name = "LoadDataBtn";
            this.LoadDataBtn.Size = new System.Drawing.Size(116, 45);
            this.LoadDataBtn.TabIndex = 0;
            this.LoadDataBtn.Text = "载入数据";
            this.LoadDataBtn.UseVisualStyleBackColor = true;
            this.LoadDataBtn.Click += new System.EventHandler(this.LoadDataBtn_click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(146, 114);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(856, 316);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Cell_onclick);
            // 
            // 信息
            // 
            this.信息.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.信息.Controls.Add(this.showmessagebox);
            this.信息.Controls.Add(this.label1);
            this.信息.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.信息.Location = new System.Drawing.Point(146, 439);
            this.信息.Name = "信息";
            this.信息.Size = new System.Drawing.Size(856, 154);
            this.信息.TabIndex = 5;
            this.信息.TabStop = false;
            this.信息.Text = "信息";
            // 
            // showmessagebox
            // 
            this.showmessagebox.BackColor = System.Drawing.SystemColors.Control;
            this.showmessagebox.Font = new System.Drawing.Font("宋体", 9F);
            this.showmessagebox.Location = new System.Drawing.Point(20, 24);
            this.showmessagebox.Multiline = true;
            this.showmessagebox.Name = "showmessagebox";
            this.showmessagebox.ReadOnly = true;
            this.showmessagebox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.showmessagebox.Size = new System.Drawing.Size(819, 120);
            this.showmessagebox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = " ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.ImageLocation = "images/logonew.jpg";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 99);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Select_Title
            // 
            this.Select_Title.AutoSize = true;
            this.Select_Title.Font = new System.Drawing.Font("微软雅黑", 26F);
            this.Select_Title.ForeColor = System.Drawing.Color.Red;
            this.Select_Title.Location = new System.Drawing.Point(140, 12);
            this.Select_Title.Name = "Select_Title";
            this.Select_Title.Size = new System.Drawing.Size(0, 46);
            this.Select_Title.TabIndex = 7;
            // 
            // searchbox
            // 
            this.searchbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchbox.Font = new System.Drawing.Font("宋体", 16F);
            this.searchbox.Location = new System.Drawing.Point(738, 69);
            this.searchbox.Name = "searchbox";
            this.searchbox.Size = new System.Drawing.Size(158, 32);
            this.searchbox.TabIndex = 10;
            this.searchbox.TabStop = false;
            // 
            // Search
            // 
            this.Search.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Search.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Search.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Search.Location = new System.Drawing.Point(916, 69);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(83, 32);
            this.Search.TabIndex = 9;
            this.Search.Text = "检索";
            this.Search.UseVisualStyleBackColor = false;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // NFC_PCWriter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1045, 601);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.searchbox);
            this.Controls.Add(this.Select_Title);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.信息);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.WrtOneDataBtn);
            this.Controls.Add(this.LoadDataBtn);
            this.Controls.Add(this.ConnectBtn);
            this.Font = new System.Drawing.Font("宋体", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.MaximizeBox = false;
            this.Name = "NFC_PCWriter";
            this.Text = "NFC智能门牌写入工具";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NFC_PCWriter_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.信息.ResumeLayout(false);
            this.信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.Button WrtOneDataBtn;
        private System.Windows.Forms.Button LoadDataBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox 信息;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Select_Title;
        private System.Windows.Forms.TextBox searchbox;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.TextBox showmessagebox;
    }
}

